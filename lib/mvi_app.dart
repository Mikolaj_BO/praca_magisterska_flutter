import 'package:flutter/material.dart';
import 'package:gym_planner_flutter/dependency_injector.dart';
import 'package:gym_planner_flutter/features/people/data/people_repository_impl.dart';
import 'package:gym_planner_flutter/features/people/domain/people_interactor.dart';
import 'package:gym_planner_flutter/features/people/ui/detail/detail_screen.dart';
import 'package:gym_planner_flutter/features/people/ui/list/people_list_screen.dart';
import 'package:gym_planner_flutter/networking/networking_config.dart';
import 'package:gym_planner_flutter/routes/navigation_routes.dart';

class MviApp extends StatelessWidget {
  final _peopleInteractor =
      PeopleInteractor(PeopleRepositoryImpl(NetworkingConfig()));

  @override
  Widget build(BuildContext context) {
    return Injector(
      peopleInteractor: _peopleInteractor,
      child: MaterialApp(
          title: "MVI example",
          home: PeopleListPage(

          ),
          routes: {
            NavigationRoutes.peopleList: (context) => PeopleListPage(),
            NavigationRoutes.peopleDetail: (context) => DetailPeoplePage()
          }),
    );
  }
}
