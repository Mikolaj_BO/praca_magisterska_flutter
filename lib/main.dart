import 'package:flutter/material.dart';
import 'package:gym_planner_flutter/mvi_app.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MviApp());
}
