import 'package:meta/meta.dart';
import 'package:gym_planner_flutter/features/people/data/model/people_payload.dart';

@immutable
class People {
  final String name;
  final String birthYear;

  People({this.name, this.birthYear});
}