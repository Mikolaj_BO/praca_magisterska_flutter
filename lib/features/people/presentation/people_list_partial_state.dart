import 'package:gym_planner_flutter/features/people/domain/model/people.dart';
import 'package:gym_planner_flutter/mvi_core.dart';

class PeopleListPartialState extends MviPartialState {}

class PeopleListLoaded extends PeopleListPartialState {
  List<People> people;

  PeopleListLoaded(this.people);
}

class PeopleListFailed extends PeopleListPartialState {}

class PeopleListLoading extends PeopleListPartialState {}
