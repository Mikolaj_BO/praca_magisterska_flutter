import 'dart:async';

import 'package:gym_planner_flutter/features/people/presentation/people_list_state.dart';
import 'package:gym_planner_flutter/mvi_core.dart';

class PeopleListView implements MviView<PeopleListState> {
  final fetchPeople = StreamController<bool>.broadcast(sync: true);

  @override
  Future tearDown() {
    return Future.wait([fetchPeople.close()]);
  }

  @override

  PeopleListState get initialState => PeopleListState.initial();
}
