import 'package:gym_planner_flutter/features/people/data/model/people_payload.dart';

abstract class PeopleRepository{
  Stream<PeoplePayload> peopleList();
}