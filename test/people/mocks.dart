import 'package:mockito/mockito.dart';
import 'package:gym_planner_flutter/features/people/data/people_repository.dart';
import 'package:gym_planner_flutter/features/people/domain/people_interactor.dart';
import 'package:gym_planner_flutter/features/people/presentation/people_list_presenter.dart';

class MockPeopleListInteractor extends Mock implements PeopleInteractor {}

class MockPeopleRepository extends Mock implements PeopleRepository {}

class MockPeoplePresenter extends Mock implements PeopleListPresenter {}
